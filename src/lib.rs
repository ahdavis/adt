//! # ADT
//!
//! `adt` is a set of Rust implementations of common abstract data types

//module imports
mod stack;

//use statements
pub use stack::stack::*;

//end of file
