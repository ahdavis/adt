/*
 * mod.rs
 * Module header for stack files
 * Created by Andrew Davis
 * Created on 1/15/2019
 * Licensed under the Lesser GNU Public License, version 3
 */

//module imports
pub mod stack;

//use statements
pub use stack::stack::Stack;

//end of file
